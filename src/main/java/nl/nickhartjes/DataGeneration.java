package nl.nickhartjes;

import lombok.extern.slf4j.Slf4j;
import nl.nickhartjes.component.Orchestrator;

@Slf4j
public class DataGeneration {

    public static void main(String... args) {
        new Orchestrator();
    }
}
